import pickle

def save_dict(dict):
    input_file = open("./out.txt", "wb")
    pickle.dump(dict, input_file)
    input_file.close()


arq_corpus   = open("./word.txt")
arq_word2vec = open("./en.txt")

embeddings = dict()

for s in arq_corpus:
    s = s.split(" ")
    if s[0] not in embeddings.keys():
        embeddings[s[0]] = [float(x) for x in s[1:]]
arq_corpus.close()



for line in arq_word2vec:
    line = line.rstrip()

    if line[0] in embeddings.keys():
        embeddings[line[0]] = line[1:]


save_dict(embeddings)