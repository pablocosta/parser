# coding=UTF-8

from gensim.models.word2vec import LineSentence, Word2Vec
from Limpar_corpus import DependencyReader
import numpy as np
import os

def gen_label(size):
    sentences = LineSentence("./corpus/arquivo_label.txt")



    model = Word2Vec(sentences, min_count=1, workers=16, size=100) # an empty model, no training

    model.save_word2vec_format(fname="./corpus/label.txt")

    arq = open("./corpus/label.txt")
    input_file = open("./corpus/label2.txt","w")

    for line in arq:
        line = line.split(" ")
        if len(line) <= 2:
            continue

        input_file.write(line[0]+" ")
        input_file.write(' '.join("%f" % float(val) for val in np.random.uniform(low=-0.01, high=0.01, size=size)))
        input_file.write("\n")


    input_file.write("NULL ")
    input_file.write(' '.join("%f" % float(val) for val in np.random.uniform(low=-0.01, high=0.01, size=size)))
    input_file.write("\n")

    input_file.close()

    del arq
    os.rename("./corpus/label2.txt", "./corpus/label.txt")
    os.system("sed -i 1d ./corpus/label.txt")



def gen_tag(size):
    sentences = LineSentence("./corpus/arquivo_pos.txt")



    model = Word2Vec(sentences, min_count=1, workers=16, size=100) # an empty model, no training

    model.save_word2vec_format(fname="./corpus/pos.txt")

    arq = open("./corpus/pos.txt")
    input_file = open("./corpus/pos2.txt","w")

    for line in arq:
        line = line.split(" ")
        if len(line) <= 2:
            continue
        input_file.write(line[0]+" ")
        input_file.write(' '.join("%f" % float(val) for val in np.random.uniform(low=-0.01, high=0.01, size=size)))
        input_file.write("\n")


    input_file.write("NULL ")
    input_file.write(' '.join("%f" % float(val) for val in np.random.uniform(low=-0.01, high=0.01, size=size)))
    input_file.write("\n")

    input_file.write("ROOT ")
    input_file.write(' '.join("%f" % float(val) for val in np.random.uniform(low=-0.01, high=0.01, size=size)))
    input_file.write("\n")

    input_file.write("UNKNOWN ")
    input_file.write(' '.join("%f" % float(val) for val in np.random.uniform(low=-0.01, high=0.01, size=100)))
    input_file.write("\n")

    input_file.close()

    del arq
    os.rename("./corpus/pos2.txt", "./corpus/pos.txt")
    os.system("sed -i 1d ./corpus/pos.txt")

corpus = DependencyReader()
corpus.load("english")
sentences = LineSentence("./corpus/arquivo_palavras.txt")




model = Word2Vec(sentences, min_count=1, workers=16, size=100) # an empty model, no training

model.save_word2vec_format(fname="./corpus/word.txt")
os.system("sed -i 1d ./corpus/word.txt")
input_file = open("./corpus/word.txt", "a")

input_file.write("NULL ")
input_file.write(' '.join("%f" % float(val) for val in np.random.uniform(low=-0.01, high=0.01, size=100)))
input_file.write("\n")

input_file.write("ROOT ")
input_file.write(' '.join("%f" % float(val) for val in np.random.uniform(low=-0.01, high=0.01, size=100)))
input_file.write("\n")

input_file.write("UNKNOWN ")
input_file.write(' '.join("%f" % float(val) for val in np.random.uniform(low=-0.01, high=0.01, size=100)))
input_file.write("\n")

gen_label(100)
gen_tag(100)

